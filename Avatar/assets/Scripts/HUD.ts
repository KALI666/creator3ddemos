import { UIController } from "./UIController";
import { UILayer, UIMgr } from "./UIMgr";
import { UI_MyInfo } from "../resources/ui/MyInfo/UI_MyInfo";
import { AvatarBodyparts } from "./AvatarBodyparts";
import { game } from "cc";

export class HUD extends UIController {

    constructor() {
        super('ui/HUD', UILayer.HUD);
    }

    protected onCreated() {

        let fn = (evt, args) => {
            game.emit(AvatarBodyparts.EVENT_CHANGE_PART, args.part,args.suit);
        }

        for (let i = 0; i < AvatarBodyparts.NUM; ++i) {
            let partName = AvatarBodyparts.getPartName(i);
            this.onButtonEvent('ops/' + partName + '/btn_004', fn, null, { part: i, suit: '004' });
            this.onButtonEvent('ops/' + partName + '/btn_006', fn, null, { part: i, suit: '006' });
            this.onButtonEvent('ops/' + partName + '/btn_008', fn, null, { part: i, suit: '008' });
        }

        //箭头函数直接监听事件，无法移除事件
        this.onButtonEvent('btn_info', () => {
            //弹出信息窗口
            UIMgr.inst.showUI(UI_MyInfo);
        });
    }
}
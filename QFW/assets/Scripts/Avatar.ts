import { _decorator, Component, Node, Prefab, Texture2D, loader, instantiate, find, SkeletalAnimationComponent, SkinningModelComponent, ImageAsset, game } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Avatar')
export class Avatar extends Component {

    start() {

    }

    onDestroy() {

    }

    update(deltaTime: number) {
        // Your update function goes here.
        let r = this.node.eulerAngles.clone();
        r.y += deltaTime * 10;
        this.node.eulerAngles = r;
    }
}
